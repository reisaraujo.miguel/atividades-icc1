/*
Aluno: Miguel Reis de Araújo
NºUSP: 12752457
*/

#include <stdio.h>
#include <stdlib.h>

#define MOORE 'M'
#define NEUMANN 'N'
#define CEL_VIVA 'x'
#define CEL_MORTA '.'

typedef struct simulacao {
	int dim_ln;
	int dim_col;
	char vizinhanca;
	char **matriz;
} Simulacao;

void atualiza_tabuleiro(Simulacao *tabuleiro);
int verificar_vizihos_moore(int linha, int coluna, Simulacao *tabuleiro);
int verificar_vizihos_neumann(int linha, int coluna, Simulacao *tabuleiro);


int main()
{
	Simulacao tabuleiro;
	int num_geracoes;

	scanf(" %d %d %d %c", &tabuleiro.dim_ln, &tabuleiro.dim_col, &num_geracoes,
		&tabuleiro.vizinhanca);

	// Validação
	if (tabuleiro.dim_ln < 1 || tabuleiro.dim_col < 1 || num_geracoes < 1 ||
		(tabuleiro.vizinhanca != MOORE && tabuleiro.vizinhanca != NEUMANN)) {

		printf("Dados de entrada apresentam erro.\n");
		return 1;
	}

	// Atribuição do estado inicial
	tabuleiro.matriz = malloc(tabuleiro.dim_ln * sizeof(char *));

	for (int i = 0; i < tabuleiro.dim_ln; i++) {
		tabuleiro.matriz[i] = malloc(tabuleiro.dim_col * sizeof(char));

		for (int j = 0; j < tabuleiro.dim_col; j++) {
			scanf(" %c", &tabuleiro.matriz[i][j]);
		}
	}

	// Chamada para o calculo do modelo correto de vizinhaça
	for (int i = 0; i < num_geracoes; i++) {
		atualiza_tabuleiro(&tabuleiro);
	}

	// Impressão do resultado final
	for (int i = 0; i < tabuleiro.dim_ln; i++) {
		for (int j = 0; j < tabuleiro.dim_col; j++) {
			printf("%c", tabuleiro.matriz[i][j]);
		}
		printf("\n");
	}

	// Fazendo a faxina
	for (int i = 0; i < tabuleiro.dim_ln; i++) {
		free(tabuleiro.matriz[i]);
	}

	free(tabuleiro.matriz);

	return 0;
}


void atualiza_tabuleiro(Simulacao *tabuleiro)
{
	// Criando a matriz para atribuir o novo estado sem alterar o antigo enquanto ele ainda
	// está sendo analisado. Foi usado alocação dinamica para evitar stack overflow já que
	// a matriz pode ser bem grande.
	char **matriz_atualizada = malloc(tabuleiro->dim_ln * sizeof(char *));

	for (int i = 0; i < tabuleiro->dim_ln; i++) {
		matriz_atualizada[i] = malloc(tabuleiro->dim_col * sizeof(char));
	}

	int cont_vizinhos_vivos = 0;

	for (int i = 0; i < tabuleiro->dim_ln; i++) {
		for (int j = 0; j < tabuleiro->dim_col; j++) {

			// verificando os estados dos vizinhos
			if (tabuleiro->vizinhanca == MOORE) {
				cont_vizinhos_vivos = verificar_vizihos_moore(i, j, tabuleiro);
			}
			else {
				cont_vizinhos_vivos = verificar_vizihos_neumann(i, j, tabuleiro);
			}

			// atualizando o estado da celula atual
			switch (tabuleiro->matriz[i][j]) {
				case CEL_VIVA:
					if (cont_vizinhos_vivos < 2 || cont_vizinhos_vivos > 3) {
						matriz_atualizada[i][j] = CEL_MORTA;
					}
					else {
						matriz_atualizada[i][j] = CEL_VIVA;
					}
					break;
				case CEL_MORTA:
					if (cont_vizinhos_vivos == 3) {
						matriz_atualizada[i][j] = CEL_VIVA;
					}
					else {
						matriz_atualizada[i][j] = CEL_MORTA;
					}
			}
		}
	}

	// desalocando matriz antiga
	for (int i = 0; i < tabuleiro->dim_ln; i++) {
		free(tabuleiro->matriz[i]);
	}

	free(tabuleiro->matriz);

	// atualizando matriz
	tabuleiro->matriz = matriz_atualizada;
}


int verificar_vizihos_moore(int linha, int coluna, Simulacao *tabuleiro)
{
	// Gambiarra necessária já que a operação de resto da divisão não funciona para
	// números negativos em C
	int linha_deslocada = linha + tabuleiro->dim_ln;
	int coluna_deslocada = coluna + tabuleiro->dim_col;

	int next_ln = (linha_deslocada + 1) % tabuleiro->dim_ln;
	int prev_ln = (linha_deslocada - 1) % tabuleiro->dim_ln;
	int next_col = (coluna_deslocada + 1) % tabuleiro->dim_col;
	int prev_col = (coluna_deslocada - 1) % tabuleiro->dim_col;
	int cont = 0;

	// verificando os vizinhos
	if (tabuleiro->matriz[next_ln][coluna] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[prev_ln][coluna] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[linha][next_col] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[linha][prev_col] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[next_ln][next_col] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[next_ln][prev_col] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[prev_ln][next_col] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[prev_ln][prev_col] == CEL_VIVA) {
		cont++;
	}

	return cont;
}


int verificar_vizihos_neumann(int linha, int coluna, Simulacao *tabuleiro)
{
	// Gambiarra necessária já que a operação de resto da divisão não funciona para
	// números negativos em C
	int linha_deslocada = linha + tabuleiro->dim_ln;
	int coluna_deslocada = coluna + tabuleiro->dim_col;

	int next_ln1 = (linha_deslocada + 1) % tabuleiro->dim_ln;
	int next_ln2 = (linha_deslocada + 2) % tabuleiro->dim_ln;
	int prev_ln1 = (linha_deslocada - 1) % tabuleiro->dim_ln;
	int prev_ln2 = (linha_deslocada - 2) % tabuleiro->dim_ln;
	int next_col1 = (coluna_deslocada + 1) % tabuleiro->dim_col;
	int next_col2 = (coluna_deslocada + 2) % tabuleiro->dim_col;
	int prev_col1 = (coluna_deslocada - 1) % tabuleiro->dim_col;
	int prev_col2 = (coluna_deslocada - 2) % tabuleiro->dim_col;
	int cont = 0;

	// verificando os vizinhos
	if (tabuleiro->matriz[next_ln1][coluna] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[next_ln2][coluna] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[prev_ln1][coluna] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[prev_ln2][coluna] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[linha][next_col1] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[linha][next_col2] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[linha][prev_col1] == CEL_VIVA) {
		cont++;
	}
	if (tabuleiro->matriz[linha][prev_col2] == CEL_VIVA) {
		cont++;
	}

	return cont;
}
