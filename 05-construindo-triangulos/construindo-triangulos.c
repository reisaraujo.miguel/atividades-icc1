#include <stdio.h>

int main()
{
    int altura, contador_linhas, contador_caracteres, cont_espacos;
    char caractere;

    scanf(" %i %c", &altura, &caractere);

    if (altura < 1 || altura > 25) {printf("Altura invalida"); return 0;}

    cont_espacos = altura;
    
    for (contador_linhas = 0; altura > contador_linhas; contador_linhas++) {
	for (int i = 1; i < cont_espacos; i++) {
	    printf(" ");
	}

	cont_espacos -= 1;
	
	contador_caracteres = contador_linhas == 0 ? 1 : contador_caracteres+2;

	for (int j = 0; j < contador_caracteres; j++) {
	    printf("%c", caractere);
	}
	
	printf("\n");
    }
	
    return 0;
}
