/*
Aluno: Miguel Reis de Araújo
NºUSP: 12752457
*/
#include <stdio.h>
#include <stdlib.h>

#define ADICIONAR 1
#define EXIBIR 2
#define PROXIMA 3
#define ANTERIOR 4
#define SAIR 5

typedef struct musica {
	char *nome_musica;
	char *nome_artista;
	int duracao;
} Musica;

typedef struct playlist {
	char *nome_playlist;
	short unsigned int qt_musicas;
	Musica *lista_de_musicas;
} Playlist;


void adicionar_nova_musica(Playlist *playlist_fornecida);
void exibir_musicas(Playlist *playlist_fornecida, Musica *musica_atual);
char *read_line();


int main()
{
	Playlist playlist_fornecida;

	playlist_fornecida.nome_playlist = read_line();
	playlist_fornecida.qt_musicas = 0;
	playlist_fornecida.lista_de_musicas = malloc(sizeof(Musica));
	Musica *musica_atual = NULL;
	short unsigned int indice_musica_atual = 0;
	short unsigned int comando;

	do {
		scanf(" %hd", &comando);
		scanf(" %*[\n]");

		switch (comando) {
			case ADICIONAR:
				adicionar_nova_musica(&playlist_fornecida);
				musica_atual = &playlist_fornecida.lista_de_musicas[indice_musica_atual];
				break;

			case EXIBIR: exibir_musicas(&playlist_fornecida, musica_atual); break;

			case PROXIMA:
				indice_musica_atual += 1;
				musica_atual = &playlist_fornecida.lista_de_musicas[indice_musica_atual];
				break;

			case ANTERIOR:
				indice_musica_atual -= 1;
				musica_atual = &playlist_fornecida.lista_de_musicas[indice_musica_atual];
				break;

			case SAIR: break;
		}

	} while (comando != 5);


	// Fazendo a faxina
	free(playlist_fornecida.nome_playlist);

	for (int i = 0; i < playlist_fornecida.qt_musicas; i++) {
		free(playlist_fornecida.lista_de_musicas[i].nome_musica);
		free(playlist_fornecida.lista_de_musicas[i].nome_artista);
	}

	free(playlist_fornecida.lista_de_musicas);

	return 0;
}


void adicionar_nova_musica(Playlist *playlist)
{
	Musica nova_musica;
	short unsigned int indice = playlist->qt_musicas;

	if (indice == 15) {
		printf("Playlist cheia!\n");
		return;
	}

	nova_musica.nome_musica = read_line();
	nova_musica.nome_artista = read_line();
	scanf("%d", &nova_musica.duracao);
	scanf(" %*[\n]");

	if (indice > 0) {
		playlist->lista_de_musicas =
			realloc(playlist->lista_de_musicas, (indice + 1) * sizeof(Musica));
	}

	playlist->lista_de_musicas[indice] = nova_musica;

	printf("Musica %s de %s adicionada com sucesso.\n", nova_musica.nome_musica,
		nova_musica.nome_artista);

	playlist->qt_musicas += 1;
}


void exibir_musicas(Playlist *playlist_fornecida, Musica *musica_atual)
{
	Musica *lista = playlist_fornecida->lista_de_musicas;
	short unsigned int qt_musicas = playlist_fornecida->qt_musicas;

	printf("---- Playlist: %s ----\n", playlist_fornecida->nome_playlist);
	printf("Total de musicas: %d\n\n", qt_musicas);

	for (int i = 0; i < qt_musicas; i++) {
		Musica *musica_sendo_mostrada = &lista[i];

		if (musica_sendo_mostrada == musica_atual) {
			printf("=== NOW PLAYING ===\n");
		}

		printf("(%d). \'%s\'\n", i + 1, lista[i].nome_musica);
		printf("Artista: %s\n", lista[i].nome_artista);
		printf("Duracao: %d segundos\n\n", lista[i].duracao);
	}
}


char *read_line()
{
	int contador_caracteres = 0;
	int indice;
	char *linha_lida = malloc(sizeof(char));

	do {
		indice = contador_caracteres;

		contador_caracteres += 1;

		if (contador_caracteres > 1) {
			linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));
		}

		scanf("%c", &linha_lida[indice]);

	} while (linha_lida[indice] != '\n' && linha_lida[indice] != '\r');

	linha_lida[indice] = '\0';

	scanf(" %*[\n]");

	return linha_lida;
}
