#include <math.h>
#include <stdio.h>

double calcular_euclides(int x, int y, int i);
double calcular_hermann(int x, int y, int i);
double calcular_diferenca(int n, int e[n][3]);

int main()
{
	int contador_baldios = 0;
	int contador_residenciais = 0;
	int contador_parques = 0;

	int num_estabelecimentos;
	scanf("%d", &num_estabelecimentos);

	if (num_estabelecimentos < 1) {
		printf("Falha no processamento dos dados.\n");
		return 0;
	}

	int estabelecimentos[num_estabelecimentos][3];
	// preenchendo a matriz com os argumentos de cada estabelecimento
	for (int i = 0; i < num_estabelecimentos; i++) {
		for (int j = 0; j < 3; j++) {
			scanf("%d", &estabelecimentos[i][j]);
			
			if (j == 2) {
				switch (estabelecimentos[i][j]) {
					case -1:
						contador_baldios += 1;
	// HADOUKEN!!
						break;
					case 0: contador_residenciais += 1; break;
					case 1: contador_parques += 1; break;
					default: printf("Falha no processamento dos dados.\n"); return 0;
				}
			}
		}
	}

	printf("Ao todo, foi passado por %d terrenos baldios, %d terrenos residenciais e %d parques.\n",
		contador_baldios, contador_residenciais, contador_parques);

	printf("A diferenca total de distancia percorrida foi de %.2f metros.\n",
		calcular_diferenca(num_estabelecimentos, estabelecimentos));

	return 0;
}


double calcular_diferenca(int n, int e[n][3])
{
	double hermann = 0;
	double euclides = 0;

	for (int i = 0; i < n; i++) {
		int prox_estab;
		if (i == n - 1) {
			prox_estab = 0;
		}
		else {
			prox_estab = i + 1;
		}

		// calculo da diferenca entre x atual e o próximo x
		int distancia_x;
		if (e[prox_estab][0] > e[i][0]) {
			distancia_x = e[prox_estab][0] - e[i][0];
		}
		else {
			distancia_x = e[i][0] - e[prox_estab][0];
		}

		// calculo da diferenca entre y atual e o próximo y
		int distancia_y;
		if (e[prox_estab][1] > e[i][1]) {
			distancia_y = e[prox_estab][1] - e[i][1];
		}
		else {
			distancia_y = e[i][1] - e[prox_estab][1];
		}

		hermann += calcular_hermann(distancia_x, distancia_y, e[i][2]);
		euclides += calcular_euclides(distancia_x, distancia_y, e[i][2]);
	}

	if (hermann > euclides) {
		return hermann - euclides;
	}
	else {
		return euclides - hermann;
	}
}


double calcular_hermann(int x, int y, int i)
{
	switch (i) {
		case -1:
			return sqrt(pow(x, 2) + pow(y, 2));
			/* a diagonal de um retângulo é dada pela raiz quadrada de (x² + y²) */
		default: return x + y;
	}
}


double calcular_euclides(int x, int y, int i)
{
	switch (i) {
		case 0: return x + y;
		default:
			return sqrt(pow(x, 2) + pow(y, 2));
			/* a diagonal de um retângulo é dada pela raiz quadrada de (x² + y²) */
	}
}
