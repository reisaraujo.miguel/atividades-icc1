/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAIUSCULA 'A'
#define MINUSCULA 'a'
#define DIRETO 1
#define INDIRETO 2

char *read_line();
int verificar_palindromo_direto(char *frase, int inicio, int final);
int verificar_palindromo_indireto(char *frase, int inicio, int final);

int main()
{
	char *frase = read_line();

	int inicio = 0;
	int final = strlen(frase) - 1;

	int palindromo = verificar_palindromo_direto(frase, inicio, final);

	switch (palindromo) {
		case DIRETO: printf("Palindromo direto\n"); break;
		case INDIRETO: printf("Palindromo indireto\n"); break;
		default: printf("Nao eh um palindromo\n");
	}

	free(frase);

	return 0;
}


int verificar_palindromo_direto(char *frase, int inicio, int final)
{
	if (inicio >= final) {
		return DIRETO;
	}

	else if (tolower(frase[inicio]) == tolower(frase[final])) {
		return verificar_palindromo_direto(frase, inicio + 1, final - 1);
	}

	else if ((isalnum(frase[inicio]) && (frase[final] == ' ' || frase[final] == '/')) ||
			 (isalnum(frase[final]) && (frase[inicio] == ' ' || frase[inicio] == '/'))) {

		return verificar_palindromo_indireto(frase, inicio, final);
	}

	else if (!isalnum(frase[inicio])) {
		return verificar_palindromo_direto(frase, inicio + 1, final);
	}

	else if (!isalnum(frase[final])) {
		return verificar_palindromo_direto(frase, inicio, final - 1);
	}

	return 0;
}


int verificar_palindromo_indireto(char *frase, int inicio, int final)
{
	if (inicio >= final) {
		return INDIRETO;
	}

	else if (tolower(frase[inicio]) == tolower(frase[final])) {
		return verificar_palindromo_indireto(frase, inicio + 1, final - 1);
	}

	else if (!isalnum(frase[inicio])) {
		return verificar_palindromo_indireto(frase, inicio + 1, final);
	}

	else if (!isalnum(frase[final])) {
		return verificar_palindromo_indireto(frase, inicio, final - 1);
	}

	return 0;
}


char *read_line()
{
	int contador_caracteres = 0;
	int indice;
	char *linha_lida = NULL;

	do {
		indice = contador_caracteres;

		contador_caracteres += 1;

		linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));

		scanf("%c", &linha_lida[indice]);

	} while (linha_lida[indice] != '\n' && linha_lida[indice] != '\r');

	linha_lida[indice] = '\0';

	scanf(" %*[\n]");

	return linha_lida;
}
