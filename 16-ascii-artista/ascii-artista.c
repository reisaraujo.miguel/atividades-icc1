/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *read_line(FILE *steam);
void imprimir_arte(char **arte, int qt_linhas);
void preencher(char **arte, char cor_antiga, char cor_nova, int x, int y, int altura, int largura);
void enquadra_arte(char *nome_do_arquivo_da_arte, int altura_do_quadro, int largura_do_quadro);


int main()
{
	char *nome_arte = read_line(stdin);
	FILE *arte = fopen(nome_arte, "r");

	char **copia_arte = NULL;

	int altura = 0;
	int indice_linhas;

	// lendo imagem
	while (!feof(arte)) {
		indice_linhas = altura;
		altura++;

		copia_arte = realloc(copia_arte, altura * sizeof(char *));

		copia_arte[indice_linhas] = read_line(arte);
	}

	fclose(arte);

	int largura = strlen(copia_arte[0]);

	printf("Arte inicial:\n");
	imprimir_arte(copia_arte, altura);

	int qt_preenchimentos;

	scanf(" %d", &qt_preenchimentos);
	scanf(" %*[\n]");

	for (int i = 0; i < qt_preenchimentos; i++) {
		char cor_nova;
		int coord_x;
		int coord_y;

		scanf(" %c %d %d", &cor_nova, &coord_x, &coord_y);

		char cor_antiga = copia_arte[coord_x][coord_y];

		// os casos de teste estão considerando "x" como sendo colunas e "y" sendo linhas ¯\_(ツ)_/¯

		preencher(copia_arte, cor_antiga, cor_nova, coord_x, coord_y, altura, largura);

		printf("\nArte apos a etapa %i:\n", i);
		imprimir_arte(copia_arte, altura);
	}

	// sobrescrevendo imagem antiga
	arte = fopen(nome_arte, "w");

	for (int i = 0; i < altura; i++) {
		fprintf(arte, "%s", copia_arte[i]);
		if (i < altura - 1) {
			fprintf(arte, "%c", '\n');
		}
	}

	fclose(arte);

	printf("\nArte enquadrada:\n");
	enquadra_arte(nome_arte, altura, largura);


	for (int i = 0; i < altura; i++) {
		free(copia_arte[i]);
	}

	free(copia_arte);
	free(nome_arte);

	return 0;
}


void preencher(char **arte, char cor_antiga, char cor_nova, int x, int y, int altura, int largura)
{
	if (x >= altura || x < 0 || y >= largura || y < 0) {
		return;
	}
	else if (arte[x][y] == cor_antiga) {
		arte[x][y] = cor_nova;

		preencher(arte, cor_antiga, cor_nova, x + 1, y, altura, largura);
		preencher(arte, cor_antiga, cor_nova, x - 1, y, altura, largura);
		preencher(arte, cor_antiga, cor_nova, x, y + 1, altura, largura);
		preencher(arte, cor_antiga, cor_nova, x, y - 1, altura, largura);
	}
}


void imprimir_arte(char **arte, int altura)
{
	for (int i = 0; i < altura; i++) {
		printf("%s", arte[i]);
		printf("\n");
	}
}


char *read_line(FILE *stream)
{
	int contador_caracteres = 0;
	int indice;
	char *linha_lida = NULL;
	int stop;

	do {
		indice = contador_caracteres;

		contador_caracteres += 1;

		linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));

		stop = fscanf(stream, "%c", &linha_lida[indice]) == EOF;

	} while (linha_lida[indice] != '\n' && linha_lida[indice] != '\r' && !stop);

	linha_lida[indice] = '\0';

	scanf(" %*[\n]");

	return linha_lida;
}


void enquadra_arte(char *nome_do_arquivo_da_arte, int altura_do_quadro, int largura_do_quadro)
{
	FILE *f_arte_ptr = fopen(nome_do_arquivo_da_arte, "r");
	if (f_arte_ptr == NULL) {
		printf("Erro na abertura do arquivo, "
			   "Você esqueceu de fechar o arquivo antes? "
			   "Ou deu free na string com o nome sem querer?\n");

		exit(EXIT_FAILURE);
	}

	int qtd_espc_comeco;
	const char *apoio;
	if (largura_do_quadro % 2 == 0) {
		qtd_espc_comeco = largura_do_quadro / 2;
		apoio = "/\\";
	}
	else {
		qtd_espc_comeco = largura_do_quadro / 2 + 1;
		apoio = "Ʌ";
	}

	for (int i = 0; i < qtd_espc_comeco; i++)
		printf(" ");
	printf("%s\n", apoio);

	printf("╭");
	for (int i = 0; i < largura_do_quadro; i++)
		printf("—");
	printf("╮\n");

	for (int i = 0; i < altura_do_quadro; i++) {
		printf("|");
		for (int j = 0; j < largura_do_quadro; j++) {
			char pixel_atual = fgetc(f_arte_ptr);
			printf("%c", pixel_atual);
		}
		printf("|");

		char quebra = fgetc(f_arte_ptr);
		if (quebra != EOF)
			printf("%c", quebra);
	};
	fclose(f_arte_ptr);

	printf("\n╰");
	for (int i = 0; i < largura_do_quadro; i++)
		printf("—");
	printf("╯\n");
}
