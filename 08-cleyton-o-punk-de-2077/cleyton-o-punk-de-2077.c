#include <stdio.h>

int main()
{
	int tape[512];
	for (int i = 0; i < 512; i++) {
		scanf("%d", &tape[i]);
	}

	printf("Saida do programa:\n");

	for (int i = 0; i < 512; i++) {
		switch (tape[i]) {
			case 0:
				i = 512;
				break;
				/* eu não sei como dar um break que quebre tanto a execução do switch case quanto do
				loop, por isso pulei a execução do loop para o final */
			case 1:
				tape[tape[i + 3]] = tape[tape[i + 1]] + tape[tape[i + 2]];
				/* aqui estou usando o valor dos três próximos endereços como endereço para os
				  valores usados  na operação ADD (essa frase ficou meio confusa, eu sei)*/
				i += 3;
				break;
			case 2:
				tape[tape[i + 3]] = tape[tape[i + 1]] * tape[tape[i + 2]];
				i += 3;
				break;
			case 3:
				if (tape[tape[i + 1]] < tape[tape[i + 2]]) {
					tape[tape[i + 3]] = 1;
				}
				else {
					tape[tape[i + 3]] = 0;
				}
				i += 3;
				break;
			case 4:
				if (tape[tape[i + 1]] == tape[tape[i + 2]]) {
					tape[tape[i + 3]] = 1;
				}
				else {
					tape[tape[i + 3]] = 0;
				}
				i += 3;
				break;
			case 5:
				i = tape[tape[i + 1]] - 1;
				// estou subtraindo 1 porque ao final dessa iteração será adicionado 1 ao valor i
				break;
			case 6:
				if (tape[tape[i + 1]] != 0) {
					i = tape[tape[i + 2]] - 1;
				}
				else {
					i += 2;
				}
				break;
			case 7:
				tape[tape[i + 2]] = tape[tape[i + 1]];
				i += 2;
				break;
			case 8:
				printf("%c", tape[tape[i + 1]]);
				i += 1;
				break;
			case 9: printf("%d", tape[tape[i + 1]]); i += 1;
		}
	}

	printf("\nEstado final da fita:\n");

	for (int i = 0; i < 512; i++) {
		printf("%d\n", tape[i]);
	}

	return 0;
}
